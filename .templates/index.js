const Generator = require('yeoman-generator');
const yosay = require('yosay');
const path = require('path');

function toPascalCase(s) {
  return s.replace(/(\-|^)([a-z])/gi, function(match, delimiter, hyphenated) {
    return hyphenated.toUpperCase();
  });
}

module.exports = class extends Generator {
  initializing() {
    this.log(yosay('Welcome to the ZUI Web Component generator!'));
    this.props = {
      componentName: '',
      componentClassName: ''
    };
  }

  prompting() {
    return this.prompt([
      {
        name: 'componentName',
        type: 'input',
        message: "What's the name of your component?",
        validate: function(input) {
          return /^[a-zA-Z0-9]+(-[a-zA-Z0-9]+)*$/.test(input) ? true : 'Your component name is invalid. Try again.';
        }
      }
    ]).then(answers => {
      this.props.componentName = answers.componentName;
      this.props.componentClassName = toPascalCase(answers.componentName);
    });
  }

  writing() {
    const packageInstallPath = path.join(this.destinationRoot(), 'packages/components', this.props.componentName);
    // meh I give up; someone else can programmatically copy files
    const files = [
      '.npmrc',
      'lab.html',
      'package.json',
      'README.md',
      'tsconfig.build.json',
      'src/_element.ts',
      'src/_element.scss',
      'src/index.ts',
      'src/css/_element.fouc.scss',
      'test/_element.test.ts'
    ];

    files.forEach(f => {
      const destFilename = f.replace(/_element/g, this.props.componentName);
      this.fs.copyTpl(path.join(this.sourceRoot(), f), path.join(packageInstallPath, destFilename), this.props);
    });
  }

  end() {
    this.log(
      yosay(
        `${this.props.componentName} has been generated. Bootstrapping it with lerna, then you can start developing.`
      )
    );
    this.spawnCommand('yarn', ['run', 'bootstrap']);
    this.spawnCommand('./node_modules/.bin/lerna', [
      'add',
      '@zywave/zui-base',
      `--scope=@zywave/${this.props.componentName}`
    ]);
  }
};
