# <%= componentName %>

## Add component as a dependency, in another project

_npm_:

```shell
> npm i --save @zywave/<%= componentName %>
```

_yarn_:

```shell
> yarn add @zywave/<%= componentName %>
```

_pnpm_:

```shell
> pnpm add @zywave/<%= componentName %>
```

## Running locally

This project uses `pnpm` as a package manager; be sure all setup and dependencies for the monorepo have been followed (instructions at the [root README.md](../../README.md))

Develop and run lab

```shell
> cd packages/<%= componentName %>
> pnpm run dev
```
