import { ZuiBaseElement } from '@zywave/zui-base';
import { html } from 'lit';
import { property } from 'lit/decorators.js';
import { style } from './<%= componentName %>-css.js';

/**
 * @element <%= componentName %>
 *
 * @slot unnamed slot - default placeholder that you can fill with your own content to be rendered within the constraints of <%= componentName %>
 * @slot baz - named slot; your own content will be rendered in the baz placeholder location inside <%= componentName %>
 *
 * @cssprop [--<%= componentName %>-prop-name=var(--zui-blue)] - Description of the CSS custom property
 */

export class <%= componentClassName %>Element extends ZuiBaseElement {
  /**
   * Key name output by component
   */
  @property({ type: String })
  key = 'Hello';

  /**
   * Value output beside `key` name
   */
  @property({ type: Number, reflect: true })
  value = 1;

  /**
   * (optional): a boolean property that will become an observed attribute once the property value changes to `true` or is absent
   */
  @property({ type: Boolean, attribute: 'compound-word' })
  compoundWord = false;

  static get styles() {
    return [super.styles, style];
  }

  render() {
    return html`
      <div>${this.key}: <span>${this.value}</span></div>
      <slot></slot>
      <slot name="baz"></slot>
    `;
  }
}

window.customElements.define('<%= componentName %>', <%= componentClassName %>Element);

declare global {
  interface HTMLElementTagNameMap {
    '<%= componentName %>': <%= componentClassName %>Element;
  }
}
