import { expect } from '@esm-bundle/chai';
import { ContentWebTestRunnerTests } from '@zywave/content-web-test-runner-tests';
it('numbers to equal', function () {
    expect(2).to.equal(2);
    expect(15).to.equal(15);
});
it('initializes as ContentWebTestRunnerTests', function () {
    var element = document.createElement('content-web-test-runner-tests');
    expect(element).to.be.instanceOf(ContentWebTestRunnerTests);
});
