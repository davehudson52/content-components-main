import { expect } from '@esm-bundle/chai';
import { <%= componentClassName %> } from '@zywave/<%= componentName %>';

it('numbers to equal', () => {
  expect(2).to.equal(2);
  expect(15).to.equal(15);
});

it('initializes as <%= componentClassName %>', () => {
  const element: <%= componentClassName %> = document.createElement('<%= componentName %>');
  expect(element).to.be.instanceOf(<%= componentClassName %>);
});
