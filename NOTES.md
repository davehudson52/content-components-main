# Notes & Questions

## Friday purpose

- join Content Apps temporarily, start work on project working with someone
- pull in Jason/QA for testing
- priority is getting the WC created and published

## labs

- http and https issue
- local package labs and a root level.net authorized lab to hit API
  - eventually write a middleware plugin using web dev server to respond to certain request; https://modern-web.dev/docs/dev-server/middleware/
    - first step intercept api call and return an object
- for WC creation, should local lab just be setup to receive a JSON object?

## organization

- structure and bundling
  - src -> development -> roll up bundles .js to package root
  - should each package roll up its own files to be prod ready?
    - alternatively, have something like zui-bundle, although doesn't probably make sense at this point
- gitignore
  - use specific files to ignore or more generic globbing pattern

## releasing

- considering keeping changesets, even latest lerna has a workflow similar to changesets
- if we keep changesets 2 options:
  - CI release
    - pluses:
      - secures sensitive info such as tokens with project variables
    - minuses:
      - haven't found a way to introduce a changeset without consumer locally running script
      - could add README.md notes for releasing
      - likely end up with workflow with hand in two worlds
        - npm run changeset
        - CI releasing
  - Local script release
    - pluses:
      - workflow is all local
      - setup a README.md for release notes
    - minuses:
      - where and how to secure store the tokens and sensitive info
