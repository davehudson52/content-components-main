namespace labs;

public static class Constants
{
    public static string ApplicationName { get; } = "labs";
    public const string ReadinessHealthCheckTag = "readiness";
}
