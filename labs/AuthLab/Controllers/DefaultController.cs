﻿namespace labs;

using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

[Authorize]
public class DefaultController : Controller
{
    [HttpGet("")]
    public IActionResult Home()
    {
        return View();
    }

    [HttpGet("[action]")]
    public IActionResult Logout()
    {
        return SignOut(OpenIdConnectDefaults.AuthenticationScheme);
    }
}

