﻿namespace labs;

using Microsoft.AspNetCore.Mvc;

[Route("[controller]")]
public class ErrorController : Controller
{
    [HttpGet("{error?}")]
    public IActionResult Error(string error)
    {
        return View(error);
    }
}
