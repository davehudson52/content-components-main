namespace labs;

using Zywave.Framework.AccessControl;

public static class Features
{
    public static ResourcePathString Root { get; } = "/WebTemplate";
    public static ResourcePathString SpecialPermission { get; } = $"{Root}/SpecialPermission";
}
