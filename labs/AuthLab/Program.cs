﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.IdentityModel.JsonWebTokens;
using Serilog;
using System.Text.Json.Serialization;
using Zywave.Auth.IdentityProvider.Registration;
using Zywave.Auth.Licenses.Extensions;
using Zywave.Auth.Profiles.Extensions;
using Zywave.Auth.SuperUsers.Contracts;
using Zywave.Framework.AccessControl;
using Zywave.Framework.ContentSecurity;
using Zywave.Framework.Correlation;
using Zywave.Framework.Extensions;
using Zywave.Framework.Resources;
using Zywave.Framework.Resources.Providers;
using Zywave.Framework.ServiceClientFactory;
using labs;
using Constants = labs.Constants;
using Microsoft.Extensions.FileProviders;
using ProxyKit;
using Microsoft.AspNetCore.Authentication;
using System.Net;
using System.Net.Http.Headers;
using Zywave.Auth.Profiles.Model;

try
{
  var webApplicationBuilder = WebApplication.CreateBuilder(args);
  webApplicationBuilder.Configuration
      .AddJsonFile("idpconfig.json")
      .AddJsonFile("appsettings.json")
      .AddEnvironmentVariables();

  await new RegistrationBuilder()
      .ConfigureRegistrationConfiguration(webApplicationBuilder.Configuration)
      .UseClientRegistration()
      .ConfigureHttpServiceHost("ConnectionStrings:HttpServiceHost")
      .Build()
      .ApplyAsync();

  webApplicationBuilder.Host.UseSerilog(
      (context, loggerConfiguration) =>
          loggerConfiguration.ReadFrom.Configuration(
              context.Configuration),
              preserveStaticLogger: true);

  webApplicationBuilder.Services.AddOptions();

  webApplicationBuilder.Services.AddDistributedMemoryCache();
  webApplicationBuilder.Services.AddSession(options =>
  {
    options.Cookie.HttpOnly = true;
    options.Cookie.IsEssential = true;
  });

  webApplicationBuilder.Services.AddHttpContextAccessor();

  webApplicationBuilder.Services.AddCorrelation();

  webApplicationBuilder.Services.Configure<ForwardedHeadersOptions>(options =>
  {
    options.ForwardedHeaders = ForwardedHeaders.XForwardedFor |
          ForwardedHeaders.XForwardedProto;
    options.KnownNetworks.Clear();
    options.KnownProxies.Clear();
  });

  //webApplicationBuilder.Services.AddL1L2RedisCache(options =>
  //{
  //    options.Configuration = webApplicationBuilder.Configuration
  //        .GetConnectionString("TransientRedis");
  //    options.InstanceName = $"{Constants.ApplicationName}.";
  //});

  //var dataProtectionBuilder = webApplicationBuilder.Services
  //    .AddDataProtection()
  //    .SetApplicationName(Constants.ApplicationName)
  //    .PersistKeysToStackExchangeRedis(
  //        ConnectionMultiplexer.Connect(
  //            webApplicationBuilder.Configuration.GetConnectionString("PersistentRedis")),
  //        $"{Constants.ApplicationName}.DataProtection-Keys");
  //if (!string.IsNullOrEmpty(webApplicationBuilder.Configuration["Certificates:DataProtectionPath"]))
  //{
  //     dataProtectionBuilder.ProtectKeysWithCertificate(
  //         new X509Certificate2(
  //             webApplicationBuilder.Configuration["Certificates:DataProtectionPath"],
  //            webApplicationBuilder.Configuration["Certificates:DataProtectionPassword"]));
  //}

  // webApplicationBuilder.Services.AddContentSecurity(options =>
  // {
  //     options.Frame.AllowedSources = new string[]
  //     {
  //     webApplicationBuilder.Configuration["Auth:Authority"],
  //     webApplicationBuilder.Configuration.GetConnectionString("Api"),
  //     "https://unpkg.com",
  //     "https://cdn.heapanalytics.com",
  //     "https://cdn.zywave.com",
  //     "https://fast.appcues.com"
  //     };
  //     options.Frame.AllowSelf = true;
  //     options.ContentTypeOptions = "nosniff";
  //     options.FrameAncestors = new FrameAncestorsOptions
  //     {
  //         AllowedSources =
  //     {
  //         webApplicationBuilder.Configuration.GetConnectionString("Api"),
  //         webApplicationBuilder.Configuration["Auth:Authority"],
  //         "https://fast.appcues.com"
  //     },
  //         AllowOnlyHttps = true,
  //         AllowSelf = true,
  //     };
  //     options.Script.AddNonce = true;
  //     options.Script.AllowSelf = true;
  //     options.Script.AllowUnsafeEval = true;
  //     options.Script.AllowedSources.AddRange(
  //         webApplicationBuilder.Configuration.GetConnectionString("Api"),
  //         webApplicationBuilder.Configuration["Cdn:Uri"],
  //         "cdn.zywave.com",
  //         "cdn.heapanalytics.com",
  //         "fast.appcues.com",
  //         "unpkg.com");
  //     options.Style.AllowSelf = true;
  //     options.Style.AllowUnsafeInline = true;
  //     options.Style.AllowedSources.AddRange(
  //         webApplicationBuilder.Configuration.GetConnectionString("Api"),
  //         webApplicationBuilder.Configuration["Cdn:Uri"],
  //         "cdn.zywave.com",
  //         "fast.appcues.com",
  //         "unpkg.com");
  // });

  webApplicationBuilder.Services.AddAntiforgery();

  webApplicationBuilder.Services.AddHealthChecks()
      // .AddRedis(
      //     webApplicationBuilder.Configuration
      //        .GetConnectionString("PersistentRedis"),
      //    name: "Persistent Redis")
      //.AddRedis(
      //    webApplicationBuilder.Configuration
      //        .GetConnectionString("TransientRedis"),
      //    name: "Transient Redis")
      .AddUrlGroup(
          new Uri($"http://{webApplicationBuilder.Configuration.GetConnectionString("HttpServiceHost")}"),
          name: "HTTP Service Host");

  webApplicationBuilder.Services
      .AddAuthentication(options =>
      {
        options.DefaultScheme = CookieAuthenticationDefaults
              .AuthenticationScheme;
        options.DefaultChallengeScheme = OpenIdConnectDefaults
              .AuthenticationScheme;
      })
      .AddCookie(options =>
      {
        options.AccessDeniedPath = $"/error/400";
        options.Events = new CookieAuthenticationEvents
        {
          OnValidatePrincipal = context =>
            {
              if (context.ShouldRejectOldTicket())
              {
                context.RejectPrincipal();
              }
              return Task.CompletedTask;
            },
        };
      })
      .AddCookieDistributeCacheTicketStore()
      .AddOpenIdConnect(options =>
      {
        webApplicationBuilder.Configuration.Bind("Auth", options);

        options.ClaimActions.MapJsonKey(JwtRegisteredClaimNames.Sub);
        options.Events = new OpenIdConnectEvents
        {
          OnRedirectToIdentityProvider = context =>
            {
              context.MapAcrValues();
              context.MapLoginHint();
              return Task.CompletedTask;
            },
          OnRemoteFailure = context =>
            {
              context.RetryOnFailure();
              return Task.CompletedTask;
            },
        };
      })
      .AddOpenIdConnectDistributedCacheStateDataFormatter();

  //   webApplicationBuilder.Services.AddKeepAlive(
  //       options =>
  //       {
  //         options.NonceProvider = context =>
  //               context.RequestServices
  //                   .GetRequiredService<IContentSecurityNonceService>()
  //                   .GetNonce();
  //       });

  webApplicationBuilder.Services.AddAccessControl()
      .AddSuperUserAclProvider()
      .AddDistributedSuperUserPermissionCache();

  webApplicationBuilder.Services.AddProfiles(
      currentProfileOptions: options =>
      {
        webApplicationBuilder.Configuration.Bind("Auth:Profile:CurrentProfile", options);
      },
      profileApiOptions: options =>
      {
        webApplicationBuilder.Configuration.Bind("Auth:Profile:ProfileApi", options);
      },
      profileManagerOptions: options =>
      {
        webApplicationBuilder.Configuration.Bind("Auth:Profile:ProfileManager", options);
      });

  //webApplicationBuilder.Services.AddMessaging()
  //    .AddRedisConnectionFactory(options =>
  //   {
  //       options.ConnectionString = webApplicationBuilder.Configuration
  //           .GetConnectionString("TransientRedis");
  //  })
  //  .AddRedisProcessingSubscriber<CurrentProfileMessageData>(options =>
  //  {
  //      options.RoutingKey = ExtensionsConstants.DefaultCurrentProfileRoutingKey;
  //  });


  webApplicationBuilder.Services.AddAuthorization(
      options =>
      {
        options.DefaultPolicy = new AuthorizationPolicyBuilder()
              .RequireAuthenticatedUser()
              .Build();

        options.AddPolicy(
              Policies.SpecialPermission,
              authorizationPolicyBuilder => authorizationPolicyBuilder
                  .RequireAuthenticatedUser()
                  .RequireAccess(
                      defaultDirective: AccessDirective.Deny,
                      resourcePath: Features.SpecialPermission));
      });

  webApplicationBuilder.Services
      .AddServiceClientFactory()
      .AddDefaultCaches()
      .AddConventionalServiceLocator(options =>
          options.Host = webApplicationBuilder.Configuration
              .GetConnectionString("HttpServiceHost"))
      .AddBuiltInServiceProxyBuilder()
      .AddHttpClient(httpClientBuilder => httpClientBuilder
          .AddHttpMessageHandler<CorrelationDelegatingHandler>())
      .AddLicensesServices()
      .AddService<ISuperUserPermissionService>();

  webApplicationBuilder.Services
      .AddLicenses()
      .AddMemoryCache();

  webApplicationBuilder.Services.AddProxy();

  webApplicationBuilder.Services.AddRouting(
      routeOptions => routeOptions.LowercaseUrls = true);
  var mvcBuilder = webApplicationBuilder.Services.AddControllersWithViews()
      .AddClaimsValueProvider()
      .AddJsonOptions(
          options =>
          {
            options.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
            options.JsonSerializerOptions.Converters.Add(
                  new JsonStringEnumConverter());
          });
  if (webApplicationBuilder.Environment.IsDevelopment())
  {
    mvcBuilder.AddRazorRuntimeCompilation();
  }

  webApplicationBuilder.Services.AddSingleton(
      serviceProvider => serviceProvider
          .GetRequiredService<IWebHostEnvironment>()
          .ContentRootFileProvider);
  webApplicationBuilder.Services.AddResources()
      .AddMemoryResourceCache(options =>
      {
        options.AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1);
      })
      .AddActionResourceContext(options =>
      {
        options.ResourceNamespacePrefix = "Views.";
      })
      .AddHttpResourceContext()
      .AddViewResourceContext(options =>
      {
        options.IncludeActionNamespace = false;
      })
      .AddLocalization(options =>
      {
        options.ProviderName = "Text";
      })
      .AddViewResourcesHelper<ViewResourcesHelper>()
      .AddJsonFileResourceProvider("Text", options =>
      {
        options.EnableCompleteCaching = true;
        options.EnableFileCacheDependencies = webApplicationBuilder.Environment.IsDevelopment();
        options.FilePathFormat = "Resources/{Name}.{Culture}.json";
        options.JsonPathFormat = "{ResourceNamespace}{Discriminator?\".$\"}";
      })
      .AddSimpleResourceContextHandler("Discriminator");

  using var webApplication = webApplicationBuilder.Build();

  webApplication.UseSerilogRequestLogging();

  if (webApplicationBuilder.Environment.IsDevelopment())
  {
    webApplication.UseDeveloperExceptionPage();
  }
  else
  {
    webApplication
        .UseExceptionHandler("/error/500");
    webApplication
        .UseStatusCodePagesWithReExecute("/error/{0}");
  }

  //webApplication.UseStaticFiles(); 
  var nodeModules = Path.Combine(Directory.GetCurrentDirectory(), "../../", "packages");
  webApplication.UseStaticFiles(new StaticFileOptions
  {
    FileProvider = new PhysicalFileProvider(nodeModules),
    RequestPath = "/packages"
  });


  webApplication.UseRouting();

  webApplication.UseForwardedHeaders();
  webApplication.UseCorrelation();
  webApplication.UseContentSecurity();

  webApplication.UseAuthentication();
  webApplication.UseAuthorization();

  webApplication.UseSession();
  webApplication.UseChooseProfile();

  webApplication.UseEndpoints(
      endpointRouteBuilder =>
      {
        endpointRouteBuilder.MapHealthChecks(
              "/health/live",
              new HealthCheckOptions
              {
                Predicate = (healthCheckRegistration) => false,
              });
        endpointRouteBuilder.MapHealthChecks(
              "/health/ready",
              new HealthCheckOptions
              {
                Predicate = (healthCheckRegistration) =>
                      healthCheckRegistration.Tags.Contains(
                          Constants.ReadinessHealthCheckTag),
              });
        endpointRouteBuilder.MapKeepAlive();
        endpointRouteBuilder.MapSwitchProfile();
        endpointRouteBuilder.MapDefaultControllerRoute();
      });

  var apiUri = new Uri(webApplicationBuilder.Configuration
      .GetConnectionString("Api"));
  webApplication.Map("/apiproxy", app =>
  {
    app.RunProxy(async context =>
      {
        var forwardContext = context
              .ForwardTo(apiUri.AbsoluteUri)
              .AddXForwardedHeaders();

        forwardContext.UpstreamRequest.Headers.Remove("Origin");

        var accessToken = await context.GetTokenAsync("access_token");
        if (string.IsNullOrWhiteSpace(accessToken))
        {
          return new HttpResponseMessage(HttpStatusCode.Unauthorized);
        }
        forwardContext.UpstreamRequest.Headers.Authorization =
              new AuthenticationHeaderValue("Bearer", accessToken);

        if (!forwardContext.UpstreamRequest.Headers.Contains("Profile"))
        {
          var profileToken = context.User.FindFirst(ProfileClaimTypes.Token)?.Value;
          if (!string.IsNullOrWhiteSpace(profileToken))
          {
            forwardContext.UpstreamRequest.Headers.Add("Profile", $"Token {profileToken}");
          }
        }

        var response = await forwardContext.Send();

        response.Headers.Remove("Content-Security-Policy");
        response.Headers.Remove("X-Frame-Options");
        foreach (var header in response.Headers)
        {
          if (header.Key.StartsWith("Access-Control-"))
          {
            response.Headers.Remove(header.Key);
          }
        }

        response.Headers.Remove("Vary");
        response.Headers.Add("Vary", "Origin");
        // https://developer.chrome.com/blog/private-network-access-preflight/
        // this is required for the Zywave VPN
        response.Headers.Add("Access-Control-Allow-Private-Network", "true");

        return response;
      });
  });

  webApplication.Run();
  return 0;
}
catch (Exception exception)
{
  Log.Fatal(exception, "Host terminated unexpectedly");
  return 1;
}
finally
{
  Log.CloseAndFlush();
}
