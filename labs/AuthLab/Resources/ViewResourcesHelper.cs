namespace labs;

using Zywave.Framework.Resources;

public class ViewResourcesHelper : ViewResourcesHelperBase
{
    public ViewResourcesHelper()
    {
        UseInitialViewContext = true;
    }

    [ResourceBinding]
    public Resource<string>? Links { get; private set; }

    [ResourceBinding]
    public Resource<string>? Text { get; private set; }
}
