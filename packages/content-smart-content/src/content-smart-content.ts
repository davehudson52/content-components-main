import { LitElement, css, html } from 'lit';
import { customElement, property, query, state } from 'lit/decorators.js';

@customElement('content-smart-content')
export class ContentSmartContent extends LitElement {
  static styles = css`
    :host {
      display: block;
      border: solid 1px gray;
      padding: 16px;
      width: 100%;
      max-width: 800px;
      background: white;
      color: green;
    }

    label {
      display: flex;
      flex-direction: column;
    }

    label span {
      font-weight: bold;
    }

    img {
      position: fixed;
      width: 100px;
      bottom: 0;
      left: 0;
      animation: smarty 5s 7s both ease-out;
    }

    @keyframes smarty {
      0% {
        transform: translateY(100%);
      }

      90% {
        transform: rotate(-360deg);
      }
    }

    .is-hidden {
      display: none;
    }
  `;

  //TODO: take json and make a UI
  // Use API helpers from ZAPI?!
  @property({ type: String, reflect: true, attribute: 'api-base-url' })
  apiBaseUrl = '';

  @query('textarea#token')
  private _textarea: HTMLTextAreaElement | undefined;

  @query('img')
  private _img: HTMLImageElement | undefined;

  @state()
  _data: string | undefined;

  #fetchSmartContent() {
    console.log(this._textarea?.value);

    // once proxy setup, what fetch call will look like
    fetch(`${this.apiBaseUrl}apiproxy/content/v2.0/smartcontent?lineOfBusiness=1`)
      .then((res) => res.json())
      .then((json) => (this._data = JSON.stringify(json, undefined, 3)));

    this._img?.classList.remove('is-hidden');
  }

  connectedCallback() {
    super.connectedCallback();

    // console.log(
    //   `Hint here's a token:   eyJhbGciOiJSUzI1NiIsImtpZCI6IjNEQjRCMTFFMzVCRjBCNEEzRDhEQjAwRTk4OUY5MjA5NkIzMUYxOTBSUzI1NiIsIng1dCI6IlBiU3hIaldfQzBvOWpiQU9tSi1TQ1dzeDhaQSIsInR5cCI6ImF0K2p3dCJ9.eyJpc3MiOiJodHRwczovL2Rldi1hdXRoLnp5d2F2ZS5jb20iLCJuYmYiOjE2NjAzMzIyODcsImlhdCI6MTY2MDMzMjI4NywiZXhwIjoxNjYwMzM1ODg3LCJhdWQiOiJaeXdhdmUuQXBpLkNvbnRlbnQiLCJzY29wZSI6WyJhcGkuY29udGVudDpyZWFkIl0sImFtciI6WyJwd2QiXSwiY2xpZW50X2lkIjoiYTUzZTBhYWEtMTA4ZS00MDc4LWE0MjQtNGZjMDIzODNjMTgwIiwic3ViIjoiMjI0ZTFhZWQtMTQ2ZS00YWRhLWE0YTUtYjQ0YTdhZTFhMDFkIiwiYXV0aF90aW1lIjoxNjYwMjMxNzgwLCJpZHAiOiJsb2NhbCIsInNpZCI6IjAxMzA3NUU2NEM3QkMxOTQ2ODQ3QUI4NURGQjM4MEQ0In0.cc-xB4tSeTmQEsjrsqEvy5QmZRNXekxyoE9LqA6reizGY_VgAm5WFAqa2ns-YDprkZY9NBjYZozG5FeeCBUU0tx2fPDbt-IP5KA3uCgFuwUbfqUWEJiqx2w3_cVITm7zGK6aTQlA68KSr7PHM5z7UN2uDcxLBpSMQe7iCuDvarccXR7uo7U14nPM-e7PsH-jWOMkj4y_LvK2HfWAWe5UduiwbUeoRERavQdz7X-np6OHCb274ETlNnrFP_ZCPR48OmydEhkC-AhJuqnw4EeiWhhWg7mmpp_C2hyBfLczFJRMWGqscj2qvkAqFCY0VtG-6NT2vssC2svMujFol8mRNQ`
    // );

    this.#fetchSmartContent();
  }

  render() {
    return html`
      <div>
        <h1>content-smart-content WC render</h1>
        <div class="data-container">
          <pre>${this._data}</pre>
        </div>
        <img
          class="is-hidden"
          src="https://ih1.redbubble.net/image.465517929.9300/st,small,507x507-pad,600x600,f8f8f8.u3.jpg"
        />
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'content-smart-content': ContentSmartContent;
  }
}
