import { expect } from '@esm-bundle/chai';
import { ContentSmartContent } from '@zywave/content-smart-content';

it('numbers to equal', () => {
  expect(2).to.equal(2);
  expect(15).to.equal(15);
});

it('initializes as ContentSmartContent', () => {
  const element = document.createElement('content-smart-content');
  expect(element).to.be.instanceOf(ContentSmartContent);
});
